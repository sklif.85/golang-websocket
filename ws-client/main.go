package main

import (
	"golang.org/x/net/websocket"
	"log"
	"fmt"
)

//TODO WS-CLIENT

var origin = "http://localhost/"
var url = "ws://localhost:12345/ws"

func main() {
	ws, err := websocket.Dial(url, "", origin)
	if err != nil {
		log.Fatal(err)
	}

	message := []byte("Gave me a sequence!")
	_, err = ws.Write(message)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Send: %s\n", message)

	for {
		var msg= make([]byte, 512)
		_, err = ws.Read(msg)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Receive: %s\n", msg)
	}
}
