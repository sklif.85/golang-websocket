package main

import (
	"fmt"
	"log"
	"net/http"
	"golang.org/x/net/websocket"
	"encoding/json"
	"time"
)
//TODO WS-SERVER



func echoHandler(ws *websocket.Conn) {
	msg := make([]byte, 512)
	n, err := ws.Read(msg)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Receive: %s\n", msg[:n])

	i := 1
	for {
		res, _ := json.Marshal(i)
		_, err := ws.Write(res)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Send: %d\n", i)
		i++
		time.Sleep(time.Second * 10)
	}
}

func main() {
	http.Handle("/ws", websocket.Handler(echoHandler))
	err := http.ListenAndServe(":12345", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}